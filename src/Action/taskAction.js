import axios from 'axios'
export const dataAction = (page, searchPattern) => {
    searchPattern = searchPattern === '' ? null : searchPattern;
    let count = 15;
    return async (dispatch) => {
        const data = await axios.get(`https://api.jikan.moe/v3/search/anime?q=${searchPattern} & limit=${count}&page=${page}`)
        if (data.status === 200) {
            const items = data.data;
            dispatch({ type: "SUCCESS_ITEMS", payload: items.results });
        }
    }
}

export const searchAction = (searchPattern, page) => {
    searchPattern = searchPattern === '' ? null : searchPattern;
    let count = 15;
    return async (dispatch) => {
        const data = await axios.get(`https://api.jikan.moe/v3/search/anime?q=${searchPattern} & limit=${count}&page=${page}`)
        if (data.status === 200) {
            const items = data.data;
            dispatch({ type: "SUCCESS_SEARCH", payload: items.results });
        }
    }
}


export const onLikeUnlikeAction = (id) => {
    let likeStatus = localStorage.getItem("likeStatus") && JSON.parse(localStorage.getItem("likeStatus"))
    if (!likeStatus) {
        likeStatus = {};
        likeStatus[id.toString()] = true
    } else {
        likeStatus[id.toString()] = likeStatus[id] ? !likeStatus[id] : true
    }
    localStorage.setItem("likeStatus", JSON.stringify(likeStatus))

    return {
        type: "SUCCES_LIKE",
        id
    }
}
