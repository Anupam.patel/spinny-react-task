import { combineReducers } from 'redux';
import { dataReducer } from '../Reducer/dataReducer';

const rootReducer = combineReducers({ dataReducer });


export default rootReducer;