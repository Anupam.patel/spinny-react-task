const initState = {
    data: [],
    liked: {}
};

export const dataReducer = (state = initState, action) => {
    switch (action.type) {
        case "SUCCESS_ITEMS":
            let temp = [...state.data];
            if (temp.length > 0) {
                action.payload.forEach(element => {
                    temp.push(element)
                });
            }
            return {
                ...state, data: temp.length > 0 ? temp : action.payload
            }
        case "SUCCESS_SEARCH":
            return {
                ...state, data: action.payload
            }
        case "SUCCES_LIKE": {
            let previousLikes = { ...state.liked };
            let likeStatus = true;

            if (previousLikes[action.id])
                likeStatus = !previousLikes[action.id]

            let updated = { ...previousLikes, [action.id]: likeStatus }
            return { ...state, ["liked"]: updated }
        }
        default:
            return {
                ...state
            }
    }
}