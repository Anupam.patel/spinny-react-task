import React, { useEffect, useState } from 'react';
import { Container, CardDeck, Row, Col, Navbar, Form, Card, Button } from "react-bootstrap";
import { dataAction, searchAction, onLikeUnlikeAction } from "../Action/taskAction";
import { connect } from "react-redux";

function Data(props) {
  const [state, setState] = useState({ searchPattern: '', page: 1, count: 15 });
  let localState = localStorage.getItem("likeStatus") ?
    JSON.parse(localStorage.getItem("likeStatus")) : props.liked;
  useEffect(() => {
    const state = { searchPattern: '', page: 1, count: 15 }
    props.dataAction(state.page, state.searchPattern)
  }, []);

  const onClickHandler = () => {
    let newState = { ...state }
    let searchPattern = newState.searchPattern;
    let page = newState.page + 1;
    setState({ ...state, page: page })
    props.dataAction(page, searchPattern)
  }

  const onChangeHandler = (e) => {
    setState({ ...state, searchPattern: e.target.value })
  }

  const onSubmitHandler = (e) => {
    e.preventDefault();
    setState({ ...state, page: 1 })
    props.searchAction(state.searchPattern, 1)
    window.scrollTo(0, 0);
  }

  const likeHandler = (id) => {
    props.onLikeUnlikeAction(id)
  }

  const { items } = props
  return (
    <div style={{ minHeight: "100%", background: '#2676bf' }}>
      <Navbar fixed="top" style={{ background: "#2676bf" }}>
        <Form onSubmit={onSubmitHandler} style={{ width: "100%", textAlign: "center" }}>
          <input className='search-input' type="text" onChange={onChangeHandler} value={state.searchPattern} placeholder="Search" />
          <button className="buttonCustom" style={{ width: "10%" }} type="submit" >Go</button>
        </Form>
      </Navbar>
      <div ></div>
      <Container style={{ paddingTop: "8%" }} >
        <Row >
          <CardDeck >
            {items && items.length > 0 ? items.map(item => (
              <Col style={{ display: "flex", marginBottom: "20px" }} key={item.mal_id} sm={4}>
                <Card style={{ borderRadius: "20px"/*,height: "100%", marginTop: '10px', marginBottom: '10px'*/ }}>
                  <Card.Img style={{ borderRadius: "20px", height: "400px" }} src={item.image_url} />
                  <Card.Body>
                    <Card.Title style={{ textAlign: "center" }}>{item.title}</Card.Title>
                  </Card.Body>
                  <Card.Title onClick={() => likeHandler(item.mal_id)} style={{ cursor: "pointer", textAlign: "center", color: "blue" }}>{localState[item.mal_id] ? 'Liked' : 'Like'}</Card.Title>
                </Card>
              </Col>
            ))
              : <h1 style={{ color: "grey" }}>No Data Found</h1>}
          </CardDeck>
        </Row>
        <div style={{ textAlign: "center" }}>
          {items.length ? <Button onClick={onClickHandler} variant="success">Load More</Button> : null}
        </div>
      </Container >
    </div >
  );
}
const mapStateToprops = ({ dataReducer }) => {
  return { items: dataReducer.data, liked: dataReducer.liked }
}

export default connect(mapStateToprops, { dataAction, searchAction, onLikeUnlikeAction })(Data);
